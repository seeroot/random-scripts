#!/usr/bin/perl
#
# Simple script to email when a dell disk drive gets an amber light
#


use strict;
use warnings;
use diagnostics;
use v5.10.1;

# Declare variables
chomp (my $hostname = `uname -n`);	# gets the machines hostname
my $sysadmin = "disk-alerts";		# this should be an alias in /etc/aliases
my $mail = "/bin/mail";			# path to the mail binary

# Hard drives
my @disk_numbers = (0,1,2);		# the disk drives starting from 0
my $count;

# Send an email for any disk which has an error
foreach my $disk (@disk_numbers){
    $count = 0;
    open(my $data, '-|', 'smartctl', '-d', "megaraid,$disk", '-a', '/dev/sda');		# ensure you have smartctl installed and that your device is /dev/sda
    while ( my $line = <$data>){
        if ($line =~ /SMART Health Status: OK/){
           $count += 1;
        }
    }
        if ($count < 1){

        # send email
        my $subject = "\" System Alert on $hostname\"";
        my $message = "There is a possible hardware error on disk number $disk, on host $hostname!  Please investigate and check for any amber lights.";
        open(MAIL,"|$mail -s $subject $sysadmin");
        print MAIL $message;
        close(MAIL);


        }

}