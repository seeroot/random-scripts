#!/usr/bin/perl

use warnings;
use strict;
use v5.24.1;

use File::Fetch;
use WWW::Mechanize;
use Archive::Extract;
use File::Find::Rule;

my $url   = "https://www.kernel.org/";

my $mech  = WWW::Mechanize->new();

$mech->get( $url );

# make an array of all the links
my @links = $mech->links();

foreach my $link (@links) {
   if ($link->url() =~ m/linux/ and $link->text() eq "tarball"){  # If the link contains linux and has a descriptio of tarball
        my ($ff, $ae);
        my $tar_file = (split m{/} => $link->url())[-1]; # Split by / and get the last item which is the filename
        mkdir($tar_file);
        #download file into directory
        my $where = $ff->fetch( to => "$tar_file" );
        chdir($tar_file);
        ### build an Archive::Extract object ###
        my $archive = Archive::Extract->new( archive => "$tar_file" );
        ### extract to cwd() ###
        #my $ok = $archive->extract;
        ### extract to /tmp ###
        my $ok = $ae->extract( to => '/tmp' );
        chdir("/home/max/Documents/Scripts");
    }
}

my @files = File::Find::Rule->file()
                            ->name( '*.c' )
                            ->in( '/home/username/Documents/Scripts' );

foreach my $file (@files) {
    say "file: $file";
}