#!/bin/bash

#############################################################################################################
DBNAME=mattermost
DATE=$(date +'%Y%m%d')
FILENAME=${DBNAME}_${DATE}.gz
LOCAL_BACKUP_LOCATION=/home/postgres/backups/
REMOTE_BACKUP_LOCATION=
ADMIN=admin

su - postgres -c "pg_dump ${DBNAME} | gzip > ${LOCAL_BACKUP_LOCATION}${FILENAME}"

if [ $? -eq 0 ]
then
    echo "Database backup successful"
    scp ${LOCAL_BACKUP_LOCATION}/${FILENAME} ${REMOTE_BACKUP_LOCATION}
else
    echo "Database backup failed, please investigate" | mail -s "Database backup failed on $(uname -n)" ${ADMIN}
fi
###############################################################################################################

FILE_BACKUP=mattermost_files_${DATE}.tar.gz
IMPORTANT_FILES=" \
/opt/mattermost/config/config.json \
/opt/mattermost/bin/data \
/etc/nginx/conf.d/mattermost.conf"

tar -zcvf ${LOCAL_BACKUP_LOCATION}${FILE_BACKUP} ${IMPORTANT_FILES}

if [ $? -eq 0 ]
then
    echo "File backup Successful"
    scp ${LOCAL_BACKUP_LOCATION}/${FILE_BACKUP} ${REMOTE_BACKUP_LOCATION}
else
    echo "File backup Failed, please investigate" | mail -s "File backup failed on $(uname -n)" ${ADMIN}
fi

#################################################################################################################