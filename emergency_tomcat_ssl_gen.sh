#!/bin/bash

# working solution to get SAN certs for Tomcat and other apps that use JAVA keystores
# this was a quick solution in an emergency so input and other things need work
# not to be used but to be seen as a template

echo "Please enter the Country"
read COUNTRY

echo "Please enter the State"
read STATE

echo "Please enter the City"
read CITY

echo "Please enter the Organization"
read ORGANIZATION

echo "Please enter the Organizational Unit"
read OU

echo "Please enter the admin email address"
read EMAIL_ADDRESS

echo "Please enter the Server's FQDN"
read FQDN

echo "Please enter the Server's IP Address"
read IP

echo "Please enter a Keystore Password"
read PASSWORD

echo "Please enter a Keystore Alias"
read ALIAS


openssl req -new -sha256 -nodes -out ${FQDN}.csr -newkey rsa:2048 -keyout ${FQDN}.key -config <(
cat <<-EOF
[req]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C=${COUNTRY}
ST=${STATE}
L=${CITY}
O=${ORGANIZATION}
OU=${OU}
emailAddress=${EMAIL_ADDRESS}
CN =${FQDN}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = ${FQDN}
EOF
)

# only to be run after getting the certificate signed, of course the variables will need to be substituted

#openssl pkcs12 -export -out ${FQDN}.pfx -inkey ${FQDN}.key -in ${FQDN}.crt

#keytool -importkeystore -srckeystore ${FQDN}.pfx -srcstoretype pkcs12 -srcalias 1 -srcstorepass ${PASSWORD} -destkeystore ${FQDN}.jks -deststoretype jks -deststorepass ${PASSWORD} -destalias ${ALIAS}