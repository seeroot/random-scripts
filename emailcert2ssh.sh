#!/bin/bash
#
# Script for setting up CAC auth
# Set up PuttyCAC to store the cert for CAPI auth
#
#

# Get the user's name
echo "Please enter the exact name of the user who needs an SSH Key"
read SSH_USER

# Get the extracted email cert
echo "Please enter the full path to the file that contains the exported email certificate"
read EXPORTED_CERT

# Get the user's home
SSH_USER_HOME=$(grep $SSH_USER /etc/passwd | cut -d: -f6)

cd $SSH_USER_HOME

openssl x509 -in $EXPORTED_CERT -pubkey -noout > temp_cert_12434.pub

ssh-keygen -f temp_cert_12434.pub -i -m PKCS8 > $SSH_USER.pub

rm -f temp_cert_12434.pub

if [ ! -d "$SSH_USER_HOME/.ssh" ]; then
    mkdir $SSH_USER_HOME/.ssh
    chmod 700 $SSH_USER_HOME/.ssh
fi

echo "$(date) $SSH_USER Email cert for CAC" >> $SSH_USER_HOME/.ssh/authorized_keys
cat $SSH_USER.pub >> $SSH_USER_HOME/.ssh/authorized_keys
chmod 600 $SSH_USER_HOME/.ssh/authorized_keys
chown $SSH_USER:$(id -g -n $SSH_USER) -R $SSH_USER_HOME/.ssh

rm -f $SSH_USER.pub