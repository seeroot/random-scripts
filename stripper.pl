#!/usr/bin/perl

use warnings;
use strict;
use v5.24.1;

use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);

# Initialize variables
my ($dnum,$unum,$lnum,$snum,$file,$wfile,$len);


# Command line arguements to parse
GetOptions(
    'digits|d=i' => \$dnum,
    'upper|u=i' => \$unum,
    'lower|l=i' => \$lnum,
    'special|s=i' => \$snum,
    'infile|i=s' => \$file,
    'outfile|o=s' => \$wfile,
) or die "Usage: $0 \n";

# Open password list as a read only filehandle
open(my $readfh, '<', $file) or die("Could not open $file");

# Open up a filehand to write the matched passwords to
open(my $writefh, '>', $wfile) or die("Could not open $wfile");

# Character classes multiplied by the required amount
my $digits = '[\d]' x $dnum;
my $uppercase = '[A-Z]' x $unum;
my $lowercase = '[a-z]' x $lnum;
my $special = '[^a-zA-Z0-9]' x $snum;
# The length of the password
my $length = 6;

# Read lines from the dictionary and write them to a new file -
# if they match the complexity requirements
while (my $line = <$readfh>){
    if ( length($line) >= $length
     && $line =~ /$digits/ && $line =~ /$uppercase/
     && $line =~ /$lowercase/ && $line =~ /$special/ ){
       # If the line contains the criteria, write the line to the file
        print $writefh $line;
    };
}