#!/bin/bash

hosts=( )

for ip in "${hosts[@]}";
do
echo "Checking cipher for ${ip}"
openssl s_client -connect ${ip}:443 < /dev/null 2>/dev/null | openssl x509 -text -in /dev/stdin | grep "Signature Algorithm"
done