# install the cdrecord and mkisofs utilities
yum install cdrecord mkiso -y

# create the iso file
mkisofs -o patches.iso -J -R -A -V -v /path/to/be/patches

# look for where the cd drive is
wodim -scanbus

# the dev should equal to the comma delimited numbers you saw in the last command
cdrecord -v speed=4 dev=10,0,0 patches.iso

# install the package necessary for growisofs
sudo yum install dvd+rw-tools

# another way to burn
growisofs -dvd-compat -Z /dev/sr0=patches.iso