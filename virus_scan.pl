#!/bin/perl
##########################################################################
#   McAfee Virus scan
#
# This script uses Mcafee command line program 'uvscan' to scan select
# files for virus.  The error checking needs work
#
#                       needs better error checking
#			very limited in what modules could be used
###########################################################################

use strict;
use warnings;
use diagnostics;
use feature 'say';
use Net::SMTP;

my @NUMLIST ;
my $max = 0 ;
my $AV_LOG = "/var/log/mcafee_scan.log" ;						# path for virus scan log
my @mail_admins = ("joe.smith\@fakecompany.com", "john.doe\@fakecompany.com");		# put admins to be emailed in case of infection
my $mailrelay = '192.168.1.1';								# can be ip address or hostname

# Change to the antivirus path
chdir('/usr/local/uvscan');

# Get the directory listing of the FTP site and create a list of the filenumbers
open (CMDOUT,"wget --spider -r --no-parent ftp://ftp:ftp\@ftp.nai.com/pub/datfiles/english/avvdat-* 2>&1 |");
while (my $line = <CMDOUT>){
    if ($line =~ m/exists/) {
        my @filenumber = $line =~ m/(\d+)/;
        push @NUMLIST, @filenumber;
    }
}

# Find the most recent file
foreach (@NUMLIST){
    if ($_ > $max){
       $max = $_ ;
    }
}

# Download only the most recent tar file
system("wget", "--connect-timeout=15", "ftp://ftp:ftp\@ftp.nai.com/pub/datfiles/english/avvdat-$max.tar");

# Extract the contents of the tar file
system("tar", "-xvf", "avvdat-$max.tar");

# Remove the tar file, now that it is not needed
unlink("avvdat-$max.tar");

# Perform a system scan
system("/usr/local/uvscan/uvscan --load=/usr/local/uvscan/host.cfg --file=/usr/local/uvscan/vscan_filesys --exclude=/usr/local/uvscan/exclude_files > $AV_LOG");  # needs work

sleep 5;

# Send the log to syslog
system("cat $AV_LOG 2>&1 | logger -t av_scan");

# Define email function
sub send_email {

    my $mail_admin = $_;
    my $smtp = Net::SMTP->new($mailrelay);
    my $nodename = `hostname -s`;
    my $hostname = `hostname -f`;

    $smtp->mail("admin\@$hostname");
    if ($smtp->to($mail_admin)) {
        $smtp->data();
        $smtp->datasend("To: $mail_admin\n");
        $smtp->datasend("\n");
        $smtp->datasend("Possible infection on $nodename, please investigate\n");
        $smtp->dataend();
    } else {
        print "Error: ", $smtp->message();
    }
    $smtp->quit;
}

# Send an email if there are any possible infections found
my $error_code = system("grep 'Possibly Infected:.............     0' $AV_LOG");

if ($error_code != 0){
    foreach (@mail_admins){
        send_email($_);
    }
}